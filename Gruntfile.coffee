module.exports = (grunt) ->
	# Load dependencies
	require('matchdep').filterDev('grunt-*').forEach(grunt.loadNpmTasks)
	# Project configuration
	grunt.initConfig
		exec:
			pandoc: 'pandoc -f markdown -t latex markdown/essay.md -o latex/essay.tex'
		latex:
			options:
				haltOnError: true
			pdf_target:
				options:
					outputDirectory: 'dist'
					engine: 'xelatex'
				src: ['latex/essay_1.tex']
		watch:
			essay:
				files: ['latex/**/*.tex', 'markdown/essay.md']
				tasks: ['build']
				options:
					spawn: false
	# Register tasks
	grunt.registerTask 'bbuild', ['latex','latex','latex']
	grunt.registerTask 'build', ['exec:pandoc', 'bbuild']
	grunt.registerTask 'default', ['build', 'watch']