# README #

Template per una rapida partenza nella stesura di una tesina o di una relazione di progetto per il CdL in Informatica Magistrale dell'Università di Bari "Aldo Moro"

Include supporto per:

- bibliografia (minima, meglio se inserite i riferimenti gestiti tramite tools come [JabRef](http://jabref.sourceforge.net/), [Mendeley Desktop](https://www.mendeley.com) o più semplicemente [Bibdesk](http://bibdesk.sourceforge.net/)) tramite il file `biblio.tex`
- appendici tramite il file `appendix.tex`

### Quali tecnologie sfrutta? ###

* [Markdown](https://daringfireball.net/projects/markdown/)
* [Pandoc](http://pandoc.org/)
* [Grunt](http://gruntjs.com/) per la compilazione automatica

### Come funziona? ###

1. clonate questo repository (o fatene un fork e miglioratelo)
2. installate le dipendenze con `npm install`
3. personalizzate titolo e altre impostazioni in `latex/essay_1.tex` 
4. modificate il contenuto nel file `markdown/essay.md`
5. compilate con `grunt build` o, se avete inserito dei riferimenti bibliografici, `grunt bbuild` 

### Contributi ###

Ovviamente sempre bene accetti, potete fare il fork e inviarmi una pull request

### Contatti ###
_coming soon_
